# People

## Setup

run `gradle build` in the project root, then `gradle bootRun`

## HTTP request

Then send a request like 

``` curl
curl --request POST \
  --url http://localhost:8080/api/people \
  --header 'content-type: multipart/form-data' \
  --form file=file
```

with the desired file.