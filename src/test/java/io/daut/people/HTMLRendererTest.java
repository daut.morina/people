package io.daut.people;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class HTMLRendererTest {
    @Test
    public void testRenderPersonPage() {
        final Person person = new Person(
                "Daut Morina",
                "Giebeleichstrasse 63",
                "8152",
                "+41 79 101 78 71",
                0,
                new GregorianCalendar(1987, Calendar.JULY, 4).getTime());

        String htmlRenderer = HTMLRenderer.renderPersonPage(List.of(person));

        assertTrue(htmlRenderer.contains("<td>Daut Morina</td>"));
    }
}
