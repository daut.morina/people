package io.daut.people;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.InputStream;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest(PeopleController.class)
public class PeopleControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testPostPeopleWithCSVFile() throws Exception {
        final InputStream csvInputStream = getClass().getResourceAsStream("/Workbook2.csv");
        final byte[] csvByteArray = IOUtils.toByteArray(csvInputStream);
        final MockMultipartFile csvFile =
                new MockMultipartFile("file", "Workbook2.csv", "text/csv", csvByteArray);

        this.mockMvc
                .perform(
                        multipart("/api/people")
                                .file(csvFile))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(7)))
                .andExpect(jsonPath("$[0].name", is("Johnson, John")));
    }

    @Test
    public void testPostPeopleWithPRNFile() throws Exception {
        final InputStream prnInputStream = getClass().getResourceAsStream("/Workbook2.prn");
        final byte[] prnByteArray = IOUtils.toByteArray(prnInputStream);
        final MockMultipartFile prnFile =
                new MockMultipartFile("file", "Workbook2.prn", "application/octet-stream", prnByteArray);

        this.mockMvc
                .perform(
                        multipart("/api/people")
                                .file(prnFile))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(7)))
                .andExpect(jsonPath("$[0].name", is("Johnson, John")));
    }
}
