package io.daut.people;

import java.util.Date;

public class Person {
    private final String name;
    private final String address;
    private final String postalCode;
    private final String phoneNumber;
    private final double creditLimit;
    private final Date dateOfBirth;

    public Person(String name, String address, String postalCode, String phoneNumber, double creditLimit, Date dateOfBirth) {
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.creditLimit = creditLimit;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }
}
