package io.daut.people;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class PeopleController {

    private final PeopleService peopleService;

    public PeopleController(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @PostMapping(value = "/api/people", consumes = "multipart/form-data")
    public Object postPeople(@RequestParam("file") MultipartFile file, @RequestParam(required = false) String output) throws IOException {
        List<Person> people = List.of();

        if ("text/csv".equals(file.getContentType())) people = peopleService.fromCSVFile(file);
        if ("application/octet-stream".equals(file.getContentType())) people = peopleService.fromPRNFile(file);

        if ("html".equalsIgnoreCase(output)) return HTMLRenderer.renderPersonPage(people);

        return people;
    }
}
