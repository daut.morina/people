package io.daut.people;

import java.util.List;

public class HTMLRenderer {
    public static String renderPersonPage(List<Person> people) {
        final String pre = "<!DOCTYPE HTML>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title>People</title>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "<tr>\n" +
                "<th>Name</th><th>Address</th><th>Postal Code</th><th>Phone</th><th>Credit Limit</th><th>DOB<th/>\n";

        final String response = pre + people
                .stream()
                .map(HTMLRenderer::renderPersonRow)
                .reduce((a, b) -> a + b).orElse("");

        return response + "</tr>" +
                "</table>" +
                "</body>\n" +
                "</html>";
    }

    private static String renderPersonRow(Person person) {
        return "<tr>\n" +
                "<td>" + person.getName() + "</td>\n" +
                "<td>" + person.getAddress() + "</td>\n" +
                "<td>" + person.getPostalCode() + "</td>\n" +
                "<td>" + person.getPhoneNumber() + "</td>\n" +
                "<td>" + person.getCreditLimit() + "</td>\n" +
                "<td>" + person.getDateOfBirth() + "</td>\n" +
                "</tr>\n";
    }
}
