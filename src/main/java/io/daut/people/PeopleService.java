package io.daut.people;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PeopleService {
    List<Person> fromCSVFile(final MultipartFile file) throws IOException {
        final Reader in = new InputStreamReader(file.getInputStream());
        final Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
        return StreamSupport.stream(records.spliterator(), false)
                .map(record -> new Person(record.get("Name"), record.get("Address"), record.get("Postcode"), record.get("Phone"), extractCreditLimitCSV(record.get("Credit Limit")), extractBirthdayCSV(record.get("Birthday"))))
                .collect(Collectors.toList());
    }

    List<Person> fromPRNFile(final MultipartFile file) throws IOException {
        final BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputStream()));

        boolean firstLineRead = false;
        int posName = 0;
        int posAddress = 0;
        int posPostcode = 0;
        int posPhone = 0;
        int posCreditLimit = 0;
        int posBirthday = 0;

        String line;
        List<Person> people = new ArrayList<>();

        while ((line = in.readLine()) != null) {
            if (!firstLineRead) {
                posName = line.indexOf("Name");
                posAddress = line.indexOf("Address");
                posPostcode = line.indexOf("Postcode");
                posPhone = line.indexOf("Phone");
                posCreditLimit = line.indexOf("Credit Limit");
                posBirthday = line.indexOf("Birthday");
                firstLineRead = true;
            } else {
                people.add(
                        new Person(
                                line.substring(posName, posAddress).trim(),
                                line.substring(posAddress, posPostcode).trim(),
                                line.substring(posPostcode, posPhone).trim(),
                                line.substring(posPhone, posCreditLimit).trim(),
                                extractCreditLimitPRN(line.substring(posCreditLimit, posBirthday).trim()),
                                extractBirthdayPRN(line.substring(posBirthday).trim())));
            }
        }

        return people;
    }

    private double extractCreditLimitCSV(String creditLimit) {
        return Double.parseDouble(creditLimit);
    }

    private Date extractBirthdayCSV(String birthday) {
        final String[] split = birthday.split("/");
        return new GregorianCalendar(Integer.parseInt(split[2]), Integer.parseInt(split[1]) - 1, Integer.parseInt(split[0])).getTime();
    }

    private double extractCreditLimitPRN(String creditLimit) {
        return Double.parseDouble(creditLimit) / 100;
    }

    private Date extractBirthdayPRN(String birthday) {
        final int year = Integer.parseInt(birthday.substring(0, 4));
        final int month = Integer.parseInt(birthday.substring(4, 6)) - 1;
        final int day = Integer.parseInt(birthday.substring(6, 8));
        return new GregorianCalendar(year, month, day).getTime();
    }
}
